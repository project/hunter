<?php

/**
 * @file
 * Contains hunter test class.
 */

namespace Drupal\hunter_test\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\file\Entity\File;

/**
 * An example controller.
 */
class HunterTestController extends ControllerBase
{
    /**
   * {@inheritdoc}
   */
  public function home()
  {
      $nodes = array();
      $view_result = views_get_view_result('frontpage', 'page');

      foreach ($view_result as $key => $item) {
          $nodes[$key]['title'] = $item->_entity->getTitle();
          $nodes[$key]['type'] = $item->_entity->getType();
          $nodes[$key]['nid'] = $item->_entity->id();
          $nodes[$key]['path'] = base_path().'jie/'.$item->_entity->id();
          $nodes[$key]['author'] = $item->_entity->getOwner()->getUsername();
          $nodes[$key]['created'] = $this->format_date($item->_entity->getCreatedTime());
          $nodes[$key]['setop'] = $item->_entity->isPromoted();
          $nodes[$key]['setstar'] = $item->_entity->isSticky();
          $nodes[$key]['term'] = \Drupal\taxonomy\Entity\Term::load($item->_entity->forum_tid)->getName();
          foreach ($item->_entity->getOwner()->get('user_picture') as $user_picture) {
              if ($user_picture->target_id) {
                  $nodes[$key]['user_picture'] = file_create_url(File::load($user_picture->target_id)->getFileUri());
              }
          }
          $nodes[$key]['comment']['status'] = $item->_entity->comment_forum->status;
          $nodes[$key]['comment']['cid'] = $item->_entity->comment_forum->cid;
          $nodes[$key]['comment']['last_comment_timestamp'] = $item->_entity->comment_forum->last_comment_timestamp;
          $nodes[$key]['comment']['last_comment_uid'] = $item->_entity->comment_forum->last_comment_uid;
          $nodes[$key]['comment']['comment_count'] = $item->_entity->comment_forum->comment_count;
          $nodes[$key]['view'] = \Drupal::config('statistics.settings')->get('count_content_views');
      }

      return view('index', array('name' => 'DrupalHunter', 'nodes' => $nodes));
  }

  /**
   * {@inheritdoc}
   */
  public function jie_add()
  {
      $form = \Drupal::formBuilder()->getForm('Drupal\user\Form\UserLoginForm');

      return view('jie_add', array('jieform' => $form));
  }

  /**
   * {@inheritdoc}
   */
  public function jie_detail($nid)
  {
      $jie_node = \Drupal\node\Entity\Node::load($nid);

      $node['title'] = $jie_node->getTitle();
      $node['type'] = $jie_node->getType();
      $node['content'] = $jie_node->body->value;
      $node['nid'] = $jie_node->id();
      $node['path'] = base_path().'jie/'.$jie_node->id();
      $node['author'] = $jie_node->getOwner()->getUsername();
      $node['created'] = $this->format_date($jie_node->getCreatedTime());
      $node['setop'] = $jie_node->isPromoted();
      $node['setstar'] = $jie_node->isSticky();
      $node['term'] = \Drupal\taxonomy\Entity\Term::load($jie_node->forum_tid)->getName();
      foreach ($jie_node->getOwner()->get('user_picture') as $user_picture) {
          if ($user_picture->target_id) {
              $node['user_picture'] = file_create_url(File::load($user_picture->target_id)->getFileUri());
          }
      }
      $node['comment']['status'] = $jie_node->comment_forum->status;
      $node['comment']['cid'] = $jie_node->comment_forum->cid;
      $node['comment']['last_comment_timestamp'] = $jie_node->comment_forum->last_comment_timestamp;
      $node['comment']['last_comment_uid'] = $jie_node->comment_forum->last_comment_uid;
      $node['comment']['comment_count'] = $jie_node->comment_forum->comment_count;
      $node['view'] = \Drupal::config('statistics.settings')->get('count_content_views');

      return view('jie_detail', array('node' => $node));
  }

  /**
   * {@inheritdoc}
   */
  public function format_date($time)
  {
      $t = time() - $time;
      $f = array(
          '31536000' => '年',
          '2592000' => '个月',
          '604800' => '星期',
          '86400' => '天',
          '3600' => '小时',
          '60' => '分钟',
          '1' => '秒',
      );
      foreach ($f as $k => $v) {
          if (0 != $c = floor($t / (int) $k)) {
              return $c.$v.'前';
          }
      }
  }
}
